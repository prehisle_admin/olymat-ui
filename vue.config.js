const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  pluginOptions: {
    electronBuilder: {
      customFileProtocol: "./",  // 解决打包后图标显示问题 https://blog.csdn.net/qq_29712303/article/details/125002316
      preload: {
        preload: 'src/preload.js',
        otherPreload: 'src/utils.js'
      },
      externals: ['yaml'], // List native deps here if they don't work
      builderOptions: {
        extraFiles: [
          {
            from: './scripts/conf_tpl.yaml',
            to: './scripts/conf_tpl.yaml'
          },
          {
            from: './olymat_cli',
            to: './olymat_cli'
          },
          // {
          //   from: './src/assets',
          //   to: './assets'
          // }

        ],
        productName: "olymat-ui",
        appId: 'olymatui',
        win: {
          "target": [
            "nsis"
          ],
          icon: 'public/favicon.ico',
          "requestedExecutionLevel": "requireAdministrator"
        },
        "nsis": {
          "installerIcon": "public/favicon.ico",
          "uninstallerIcon": "public/favicon.ico",
          "include": "nsis-custom.nsh",
          "createDesktopShortcut": true,
          "createStartMenuShortcut": true,
          "allowToChangeInstallationDirectory": true, //
          "oneClick": false,
          "perMachine": true,
          "artifactName": "${productName}-Setup-${version}.exe"
        },
        "extraResources": [{
          "from": "./src/assets",
          "to": "assets",
        }],

        // "asar": false,
      },
      // nodeModulesPath: ['../../node_modules', './node_modules']
    }
  },
})
