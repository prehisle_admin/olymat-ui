RequestExecutionLevel admin

!macro customRemoveFiles
  RMDir /r "$INSTDIR\locales"
  RMDir /r "$INSTDIR\resources"
  RMDir /r "$INSTDIR\swiftshader"
  Delete "$INSTDIR\*.*"
!macroend

!macro customInit
    ReadRegStr $0 HKLM "${INSTALL_REGISTRY_KEY}" "InstallLocation"
    ${If} $0 != ""
        StrCpy $INSTDIR $0
    ${Else}
        StrCpy $INSTDIR "C:\olymat-ui" ; Default dir if there was no path in registry
    ${EndIf}
    SetShellVarContext all
!macroend

