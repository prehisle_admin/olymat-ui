
const { contextBridge } = require('electron')
const utils = require('./utils')
contextBridge.exposeInMainWorld('local_api', {
  ...utils
})
