const {spawn} = require('child_process');
const path = require('path');
const process = require('process');
const fs = require('fs')
const YAML = require('yaml')
const {ipcRenderer, remote} = require("electron");
const treeKill = require('tree-kill');

module.exports = {
    createConfByTemplateV2, // V2版本通过模板创建配置文件函数
    getConfigFileListV2,  // V2版本获取配置文件列表函数
    getTplFileListV2,   // V2版本获取模板文件列表函数
    execCmd, // 执行命令
    getCwd,
    getModuleDir,
    getOlymCliExePath, // 获取olymat_cli.exe路径
    getOlymCliPath, // 获取olymat_cli
    getScriptRepositoryForUi, // 获取脚本库配置
    getScriptRepositorysConfConent,
    saveScriptRepositorysConfConent,
    getToolsSetInfos,
    getFuncList,
    createConfByTemplate,
    updateConfByData,
    removeConfFile,
    isDirExist,
    isFileExist,
    getScriptDir,
    getConfigContent,
    winMax,
    winMin,
    winClose,
    windowBusyStart,
    windowBusyStop,
    windowFinishBusy,
    first_run_init,
    getAppVersion
};

function getAppVersion() {
    return remote.app.getVersion()
}

function winMax() {
    ipcRenderer.invoke('window-max')
}

function winMin() {
    ipcRenderer.invoke('window-min')
}

function winClose() {
    ipcRenderer.invoke('window-close')
}

function windowBusyStart() {
    ipcRenderer.invoke('window-startbusy')
}

function windowBusyStop() {
    ipcRenderer.invoke('window-stopbusy')
}

function windowFinishBusy() {
    ipcRenderer.invoke('window-finishbusy')
}


function first_run_init() {
    let confPath = path.join(getCwd(), "scripts/conf.yaml")
    if (!isFileExist(confPath)) {
        fs.mkdir('js', (err) => {
            if (err) {
                console.log('出错')
            } else {
                console.log('未出错')
            }
        })
        saveScriptRepositorysConfConent(`scripts: 
  - name: "olymat官方示例、工具库"
    git_repository_url: "https://gitee.com/prehisle_admin/olymat-tools.git"
`)
    }

}

function execCmd(cmd, args, options, onData, onExit) {
    const exec = spawn(cmd, args, options)
    exec.stdout.on('data', (data) => {
        onData(data)
    })
    exec.stderr.on('data', (data) => {
        onData(data)
    })
    exec.on('exit', (code) => {
        // if (onExit) onExit(code)
        if (onExit) {
            onExit(code)
        }

    })
    exec.on('error', e => {
        console.log("error:", e)
        if (onExit) onExit(-1)
    })
    return {
        kill: () => {
            // exec.stdin.pause()
            // exec.kill()
            treeKill(exec.pid)
            console.log("execCmd kill")
        }
    }
}


// 打包后 getCwd AppData\Local\Programs\olymatui
// 调试 olymatui3
function getCwd() {
    return process.cwd()
}

// 打包后 getModuleDir olymatui\resources\app.asar
// 调试 olymatui\dist_electron
function getModuleDir() {
    return __dirname
}

function getOlymCliExePath() {
    // return path.join(getCwd(), "olymat_cli/olymat_cli.exe")
    return path.join(getCwd(), "olymat_cli", getOlymCliExePathT(), "olymat_cli.bat")
}

function getOlymCliPath() {
    // return path.join(getCwd(), "olymat_cli/olymat_cli.exe")
    return path.join(getCwd(), "olymat_cli", getOlymCliExePathT())
}

function getScriptDir() {
    return path.join(getCwd(), "scripts")
}

function getScriptRepositorysConf() {
    let confPath = path.join(getCwd(), "scripts/conf.yaml")
    const yamlContent = fs.readFileSync(confPath, 'utf-8')
    return YAML.parse(yamlContent)
}

function getScriptRepositorysConfConent() {
    let confPath = path.join(getCwd(), "scripts/conf.yaml")
    const yamlContent = fs.readFileSync(confPath, 'utf-8')
    return yamlContent
}

function saveScriptRepositorysConfConent(data) {
    let confPath = path.join(getCwd(), "scripts/conf.yaml")
    fs.writeFileSync(confPath, data, 'utf-8')
}

function getScriptRepositoryForUi() {
    let conf = getScriptRepositorysConf()
    var mapped = conf.scripts.map(item => ({
        [item.name]: {
            ...item,
            root_dir: getGitRepositoryDir(item.git_repository_url)
        }
    }));
    var newObj = Object.assign({}, ...mapped);
    return newObj
}


function getGitRepositoryDir(git_url) {
    var repositorysDir = git_url.match(/.*\/(.*).git$/m)
    return path.join(getCwd(), "scripts", repositorysDir[1])
}

function getToolsSetInfos(git_url, branch) {
    branch = branch ? branch : ""
    const suffix_branch = branch ? "." + branch : ""
    const git_repos_dir = getGitRepositoryDir(git_url) + suffix_branch
    return getToolsSetInfosByGitRepositoryDir(git_repos_dir)
}


function isDirExist(dir_path) {
    return fs.existsSync(dir_path)
}

function isFileExist(file_path) {
    return fs.existsSync(file_path)
}

function getToolsSetInfosByGitRepositoryDir(git_repos_dir) {

    const files = fs.readdirSync(git_repos_dir)

    const dirs = files.filter(fileName => {
        return fs.statSync(path.join(git_repos_dir, fileName)).isDirectory() &&
            fileName.startsWith("test_") &&
            fs.existsSync(path.join(git_repos_dir, fileName, "olymat_ui", "conf.yaml"))
    })

    var mapped = dirs.map(item => {
        const conf_path = path.join(git_repos_dir, item, "olymat_ui", "conf.yaml")
        const file_content = fs.readFileSync(conf_path, 'utf-8')
        const yaml = YAML.parse(file_content)
        return {
            [yaml.main_name]: {
                name: yaml.main_name,
                toolSetDir: path.join(git_repos_dir, item),
                git_repos_dir: git_repos_dir,
            }
        }
    })

    var newObj = Object.assign({}, ...mapped);
    return newObj
}


function getFuncList(tool_group_dir) {
    const conf_path = path.join(tool_group_dir, "olymat_ui", "conf.yaml")
    const file_content = fs.readFileSync(conf_path, 'utf-8')
    const fucn_list = YAML.parse(file_content)["func_list"]
    const mapped = fucn_list.map(item => {
        return {
            [item.name]: {
                ...item,
            }
        }
    })
    const newObj = Object.assign({}, ...mapped);
    return newObj
}

function getConfigFileListV2(tool_group_dir) {
    const conf_path = path.join(tool_group_dir, "confs")
    let files = fs.readdirSync(conf_path)

    files = files.filter(fileName => {
        return fs.statSync(path.join(conf_path, fileName)).isFile() &&
            fileName.endsWith(".yaml") && fileName !== "template.yaml" &&
            !fileName.startsWith('_tpl_') // 添加这一行，过滤以'_tpl_'开头的文件
    })

    if (files.length == 0) {
        return []
    }

    const mapped = files.map(item => {
        return {
            [item]: {
                "name": item
            }

        }
    })
    const newObj = Object.assign({}, ...mapped);
    return newObj
}

function getTplFileListV2(tool_group_dir) {
    const conf_path = path.join(tool_group_dir, "confs")
    let files = fs.readdirSync(conf_path)

    files = files.filter(fileName => {
        return fs.statSync(path.join(conf_path, fileName)).isFile() &&
            fileName.endsWith(".yaml") &&
            (fileName === "template.yaml" || fileName.startsWith('_tpl_')) // 修改过滤条件，仅保留template.yaml或者以'_tpl_'开头的文件
    })

    if (files.length == 0) {
        return []
    }

    const mapped = files.map(item => {
        let name = item.replace(/^_tpl_/, '');
        console.log("name:" + name)
        return {
            [name]: {
                "name": name
            }
        }
    })
    const newObj = Object.assign({}, ...mapped);
    return newObj
}

function createConfByTemplate(tool_group_dir, newConfName) {
    const src_path = path.join(tool_group_dir, "confs", "template.yaml")
    const dst_path = path.join(tool_group_dir, "confs", newConfName + ".yaml")
    fs.copyFileSync(src_path, dst_path);
}

function createConfByTemplateV2(tool_group_dir, tplName, newConfName) {
    let src_path = path.join(tool_group_dir, "confs", tplName)
    if (!fs.existsSync(src_path)) {
        src_path = path.join(tool_group_dir, "confs", "_tpl_" + tplName)
    }
    // 检查newConfName是否已经包含".yaml"，如果已经包含，则无需再次添加
    if (!newConfName.endsWith(".yaml")) {
        newConfName = newConfName + ".yaml";
    }
    const dst_path = path.join(tool_group_dir, "confs", newConfName)
    console.log("src_path" + src_path)
    console.log("dst_path" + dst_path)
    fs.copyFileSync(src_path, dst_path);
}

function updateConfByData(tool_group_dir, newConfFileName, Data) {
    const dst_path = path.join(tool_group_dir, "confs", newConfFileName)
    fs.writeFileSync(dst_path, Data, 'utf-8')
}

function removeConfFile(tool_group_dir, confFileName) {
    const dst_path = path.join(tool_group_dir, "confs", confFileName)
    fs.unlinkSync(dst_path)
}


function getConfigContent(tool_group_dir, confFileName) {
    const dst_path = path.join(tool_group_dir, "confs", confFileName)
    return fs.readFileSync(dst_path, 'utf-8')
}


function get_version(version_str) {
    const pieces = version_str.split("_")
    const last = pieces[pieces.length - 1]
    const vers = last.split(".").map(v => parseInt(v))
    return vers
}

function compare_ver(ver1, ver2) {
    if (ver1[0] > ver2[0]) {
        return -1;
    }
    if (ver1[1] > ver2[1]) {
        return -1;
    }
    if (ver1[2] > ver2[2]) {
        return -1;
    }
    return 1;
}

function getOlymCliExePathT() {
    const olymat_cli_path = path.join(getCwd(), "olymat_cli")
    const files = fs.readdirSync(olymat_cli_path)
    const dirs = files.filter(fileName => {
        return fs.statSync(path.join(olymat_cli_path, fileName)).isDirectory()
    })
    return dirs.sort((a, b) => {
        return compare_ver(get_version(a), get_version(b))
    })[0];
}


// console.log(getOlymCliExePathT())
// const str = 'olymat_cli_1.0.1'
// console.log(get_version(str))
// console.log(compare_ver([ 1, 0, 123 ], [ 1, 0, 22 ]))
// console.log(compare_ver([ 1, 0, 2 ], [ 1, 0, 22 ]))

