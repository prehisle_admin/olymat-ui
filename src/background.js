'use strict'

import { app, protocol, BrowserWindow, ipcMain,Tray, Menu } from 'electron'
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib'
import installExtension, { VUEJS_DEVTOOLS } from 'electron-devtools-installer'
import utils from "@/utils";
const isDevelopment = process.env.NODE_ENV !== 'production'
const path = require('path')
// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([
  { scheme: 'app', privileges: { secure: true, standard: true } }
])
let appTray = null;
let is_production = app.isPackaged
let assset_dir = ""
if (is_production) {
  assset_dir = path.join(__dirname, "../assets/")
} else {
  assset_dir = path.join(__dirname, "assets/")
}


async function createWindow() {
  // Create the browser window.
  const win = new BrowserWindow({
    width: 1024,
    height: 768,
    frame: false,
    icon: path.join(assset_dir,"logo.png"),
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
      preload: path.join(__dirname, 'preload.js'),
    },
    skipTaskbar:false
  })

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    await win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
    if (!process.env.IS_TEST) win.webContents.openDevTools()
  } else {
    createProtocol('app')
    // Load the index.html when not in development
    win.loadURL('app://./index.html')
  }

  // 1. 收到渲染进程的窗口最小化操作的通知，并调用窗口最小化函数，执行该操作
  ipcMain.handle('window-min',function(){
    win.minimize();
  })
  // 2. 窗口 最大化、恢复
  ipcMain.handle('window-max',function () {
    if(win.isMaximized()){ // 为true表示窗口已最大化
      win.restore();// 将窗口恢复为之前的状态.
    }else{
      win.maximize();
    }
  })
  // 3. 关闭窗口
  ipcMain.handle('window-close',function (){
    win && win.close();
  })

    //系统托盘图标闪烁
    var count = 0,timer = null, busy=false;
    timer=setInterval(function() {
      if (busy) {
        count++;
        let y = count%4
        try {
          if (y === 0) {
            appTray.setImage(path.join(assset_dir, "1.png"))
          } else if (y === 1) {
              appTray.setImage(path.join(assset_dir, "2.png"))
          } else if (y === 2) {
              appTray.setImage(path.join(assset_dir, "3.png"))
          } else {
              appTray.setImage(path.join(assset_dir, "4.png"))
          }
        } catch (error) {
          // console.error("发生了一个错误:", error.message);
        }

      }

    }, 600);

  ipcMain.handle("window-startbusy", function() {
    busy = true
  })

  ipcMain.handle("window-stopbusy", function() {
    busy = false
    console.log("window-stopbusy", appTray)
    try {
      appTray.setImage(path.join(assset_dir, "logo.png"))
    } catch (error) {
      // console.error("发生了一个错误:", error.message);
    }
  })

  ipcMain.handle("window-finishbusy", function() {
    busy = false
    try {
      appTray.setImage(path.join(assset_dir, "finish.png"))
    } catch (error) {
      // console.error("发生了一个错误:", error.message);
    }

  })

  // Menu.setApplicationMenu(null);
  // 任务栏图标

    //系统托盘右键菜单
    var trayMenuTemplate = [
        {
            label: '显示OLYMAT-UI',
            click: function () {
                //ipc.send('close-main-window');
                 win.show();
            }
        },
        {
            label: '退出OLYMAT-UI',
            click: function () {
                //ipc.send('close-main-window');
                 app.quit();
            }
        }
    ];

    appTray = new Tray(path.join(assset_dir, "logo.ico"));

    //图标的上下文菜单
    const contextMenu = Menu.buildFromTemplate(trayMenuTemplate);

    //设置此托盘图标的悬停提示内容
    appTray.setToolTip('欢迎使用OLYMAT-UI');

    //设置此图标的上下文菜单
    appTray.setContextMenu(contextMenu);

        //单点击 1.主窗口显示隐藏切换 2.清除闪烁
    appTray.on("click", function(){

          //主窗口显示隐藏切换
          win.isVisible() ? win.hide() : win.show();

    })
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    require('vue-devtools').install()
  }
  createWindow()
})

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', (data) => {
      if (data === 'graceful-exit') {
        app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      app.quit()
    })
  }
}

