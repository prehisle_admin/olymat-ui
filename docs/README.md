## 安装node库

```
npm install -g yarn
yarn install
```

## 引入olymat_cli

```
mklink /j .\olymat_cli ..\olymat\olymat_cli
```

# 开发调试

yarn dev

# 编译

yarn build

# 待办

- [ ] 执行时间长的操作，增加loading图示
- [ ] 支持增加工作区

